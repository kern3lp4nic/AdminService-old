'use strict';
const db= require("./DatabaseInteraction/DatabaseInteraction.js");
const table="firm";
const logTable="guest_interaction";
const schemaGuest = require('./JSONschema/Guest.json');
const schemaFirm = require('./JSONschema/Firm.json');
const jsontest = require("./TestJson.js");
// Error during operation

module.exports = {
    /*  limit: number of firms returned
        callback function of exectuting at the end
        err,res 
        err !not null -> res null
        err null -> res :[]*/
    getFirms: function(callback) { //testes
        db.query(table,100,null,null,callback); //da sistemare discorso limit
    },
    insertFirm: function(firm,callback) {// perchè limit??
        db.insert(table,firm,callback);//replace the obj
    },
    updateFirm: function(firm,callback) { //tested
        db.insert(table,firm,callback);
    },
    addGuest: function(nameFirm,guest,callback){ //tested
            db.read(table,{"name":nameFirm}, function(err,res){
                if(!err && (Object.keys(res).length > 0)){
                    db.update(table,
                    {"name":nameFirm},
                    "SET guests = list_append(if_not_exists(guests, :empty_list) , :val)",
                    {":val":[guest],":empty_list":[]},
                    function(err,res){callback(err,res);});
                }
                else
                    callback({Error:"Firm not exists",TypeError:"updateError"},null);
        });
    },
    exsistsFirm: function(name,callback) { //tested
        db.read(table,{"name":name},callback);
    },
    exsistsGuest: function(name,callback) { //tested
        db.query(table, 1, "nameg = :a", {":a" :{"S":name}},
            function(err,res){
                if (err)
                    callback(err,null);
                else{
                    var result = {};
                    if(res.length > 0)
                        result=res[0];
                    callback(null,result);
                }
            }
        );
    },
    /*getGuestConversation: function(firm, guest,callback) {//tested 
        db.query(interactionTable,100,"firm = :firm and guest = :guest",{":firm":firm,":guest":guest},callback);
    },
    addInteraction: function(sessionId,firm, guest, questionText, answerText , callback) { // da testare
        db.update(interactionTable,
        {"firm":firm,"guest":guest},
        "SET logg = list_append(if_not_exists(logg, :empty_list) , :val)",
        {":val":[{"session_id":sessionId,"question":questionText,"answer":answerText}],":empty_list":[]},
        function(err,res){callback(err,res);});
    },*/
    validateGuest : function(obj){ //tested
        return jsontest.test(schemaGuest,obj);
    },
    validateFirm : function(obj){ //tested
        return jsontest.test(schemaFirm,obj);
    },

    /*take timestamp - session id - question - answer -> SFRUTTA UPDATE */
    /* TORNO ULTIMA QUESTION */
    doLogAnswer: function(timestamp,sessionID,questionObj,answerObj,callback){
        db.update(logTable,{"sessionId":sessionID},
        "SET logg = list_append(if_not_exists(logg, :empty_list) , :val)",
        {":val":[{"timestamp":timestamp,"question":questionObj,"answer":answerObj}],":empty_list":[]},
        callback);

    },
    /**/
    getLastQuestion: function(sessionID,callback){
         db.read(logTable,{"sessionId":sessionID},function(err,res){
            var back=null;
            if(!err){
               if (Object.keys(res).length === 0) {
                  back = res;
               } else {
                  back = res.logg[res.logg.length - 1].question;
               }
            }
            callback(err,back);
         });
    }
    /*update Firm name (name,session)*/
    /*update Guest name (name,session)*/


}
