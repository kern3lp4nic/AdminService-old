'use strict';
var chai = require("chai");
var assert = chai.assert;
var expect = chai.expect;

chai.config.includeStack = true;

var database = require("./index.js");
var table="databasetest";
var correctObj={"name":"ciao","guests":[]};
var falseObj={"neme":"sdafdas", "guests":[]};

describe("DatabaseInteraction", function () {
    describe("#Insert", function () {
        context("connection ok && table name correct && key correct", function () {
            it("connection ok && table name correct && key correct", function () {
                database.insert(table, correctObj , function (err,data) {
                    console.log(err,data);
                    expect(err.Error).to.equal(null);
                    expect(data).to.equal(null);
                    expect(err.TypeError).to.equal(null);
                })
            });
        });
        context("returns error for false key", function () {
            it("returns error for false key", function () {
                database.insert(table, falseObj , function (err,data) {
                    expect(data).to.equal(null);
                    expect(data.Error).to.notEqual(null);
                    expect(data.TypeError).to.notEqual(null);
                })
            });
        });
        context("returns a valid JSON object on override", function () {
            it("returns a valid JSON object on override", function () {
                database.insert(table, correctObj , function (err,data) {
                    expect(data).to.equal(null);
                    expect(err.Error).to.equal(null);
                    expect(err.TypeError).to.equal(null);
                })
            });
        });
    });
    
    describe("#Update", function () {
        var table="ServiceTest";
        var correctKey={};
        var falseKey={};
        var updateExpression="";
        var replace="";

        context("works and update", function () {
            it("update successo", function () {
                database.update(table, correctKey, updateExpression, replace, function (err, data) {
                    expect(data).to.equal(null);
                    expect(err.Error).to.equal(null);
                    expect(err.TypeError).to.equal(null);
                })
            });
        });
        context("works and update", function () {
            it("returns error for false key", function () {
                database.update(table, falseKey, updateExpression, replace, function (err, data) {
                    expect(data).to.equal(null);
                    expect(err.Error).to.notEqual(null);
                    expect(err.TypeError).to.notEqual(null);
                })
            });
        });
    });

});

