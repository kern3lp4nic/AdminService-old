'use strict';
const fs = require("../AdminService.js");

var chai = require("chai");
var assert = chai.assert;
var expect = chai.expect;

chai.config.includeStack = true;

var admin = {username:"primo", email:"primo@test.it", password:"primo111", superadmin:true};
var admin2 = {username:"secondo", email:"secondo@test.it", password:"secondo2", superadmin:false};
var falseAdmin = {usr:"male"};
//
describe("Admin", function () {
    context("addAdmin", function () {
    
        it("Add a not existing admin", function (done) {
            fs.addAdmin(admin, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.equal(null);
                 done();
            });
        });
        it("Add an existing admin", function (done) {
            fs.addAdmin(admin, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.equal(null);
                done();
            });
        });        
    });
    context("getAdmins", function () {
        it("get all the admins", function (done) {
            fs.getAdmins(function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.not.equal(null);
                done();
            });
        });
    });
    context("updateAdmin", function () {
        it("update admin", function (done) {
            fs.updateAdmin(admin, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.equal(null);
                done();
            });
        });
        /*it("update not existing admin", function (done) {
            fs.updateAdmin(admin2, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.equal(null);
                done();
            });
        });*/
    });
    /*context("sendEmail", function () {
        it("verify email for password recovery", function (done) {
            fs.sendEmail(admin.email, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.not.equal(null);
                done();
            });
        });
    });*/
    //passwordRecoveyToken
    var token="";
    context("login", function () {
        it("verify user e password", function (done) {
            fs.login(admin.username, admin.password, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.not.equal(null);
                
                token=data.token;
                done();
            });
        });
    });
    context("verifyLogin", function () {
        it("all ok", function (done) {
            fs.verifyLogin(token, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.not.equal(null);
                done();
            });
        });
    });
    context("logout", function () {
        it("logout", function (done) {
            fs.logout(token, admin.username, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.equal(null);
                done();
            });
        });
    });
    //verifyLogin
    /*context("deleteAdmin", function () {
        it("delete an admin", function (done) {
            fs.deleteAdmin({"email": "primo@test.it"}, function (err, data) {
                expect(err).to.equal(null);
                expect(data).to.equal(null);
                done();
            });
        });
    });*/
});