'use strict';

const db = require("./DatabaseInteraction/DatabaseInteraction.js");
const table ="admin";
const crypto = require('crypto');
const jsontest = require("./TestJson.js");
//const codekey =  'lekfkghfkhfjhfkditc';

module.exports = {
    /*PRE: admin (username,email,password,superadmin)*/ 
    addAdmin: function(admin, callback) {//deve essere attinente al modello adminservice e deve aver la password
        //codifico la password
        var adminIns = {
            username:admin.username,
            email:admin.email,
            superadmin : admin.superadmin 
        };
        adminIns.password=crypto.createHash('md5').update(admin.password).digest("hex");

        db.insert(table, adminIns, callback);
    },

    /*PRE: admin (username,email,password) no required password*/ 
    updateAdmin: function(admin, callback) { 
        //devo modificarlo e controllare che se c'è la password la codifico, 
        //ma la modifica va fatta a singole proprietà e devo anche controllare che il token corrisponda a quell'username nello strato precedente
        //non tengo in considerazione admin perchè uno nasce e muove o admin o superadmin
        var updExp = "SET email = :mail";
        var valueExp = {":mail" : admin.email};

        if(admin.hasOwnProperty('password')){
            updExp = updExp+" , password = :pw";
            valueExp[":pw"] = crypto.createHash('md5').update(admin.password).digest("hex");
        }

        db.update(table, {"username":admin.username}, updExp ,valueExp ,callback);
    },
    /*Pre: username is a string*/
    deleteAdmin: function(username, callback) {
        db.remove(table, {"username":username}, callback);
    },/*POST: if exist delete else error*/

    getAdmins: function(callback) {
        db.queryProp(table,"username , email, superadmin", null, null, callback);
    },
    sendEmail: function(mail, callback) {
       
    },
    passwordRecoveyToken: function(token, mail, password, callback) {
      
    },

    logout: function(token ,username, callback) {
        db.conditionUpdate(table, {"username":username}, "SET tkn = :val", "tkn = :val2",{":val":" ",":val2":token} ,callback);
    },

    login: function(username, password, callback) {
        //codifico la password
        var pass = crypto.createHash('md5').update(password).digest("hex");

        db.queryProp(table, "username , email , superadmin" , "username = :ml and password = :psw", {":ml":username, ":psw": pass}, 
        function(err, res) {
            var result = null;
            if(!err && res.length > 0){
                var token = crypto.createHash('md5').update((new Date().getTime()+username)).digest("hex");
                result=null;
                db.update(table,{"username":username},"SET tkn = :val",{":val":token}, function(err,res2){
                    if(!err){
                        result = res[0];
                        result.token = token;
                    }
                    callback(err, result);
                });
            }else{
                err = {Error:"Invalid administrator credential", TypeError:"InvalidAdminCredential"};
                callback(err, null);
            }
        });
    },
    verifyLogin: function(token, callback) { //ok non toccare da testare ma funzionante
        db.queryProp(table, "username , email , superadmin" , "tkn = :tokn", {":tokn":token}, function(err,res){
            var result=null;
            if( !err && res.length > 0) {
                result = res[0];
            }else
                err = {Error:"Invalid administrator token", TypeError:"InvalidAdminToken"};
            callback(err,result);
        });
    }, 
    validateAdmin: function(obj){
         return jsontest.test(require('./JSONschema/Admin.json'),obj);
    },
    validateAuth: function(obj){
         return jsontest.test(require('./JSONschema/Auth.json'),obj);
    },
    validateUpdate: function(obj){
         return jsontest.test(require('./JSONschema/UpdateAdmin.json'),obj);
    }

}