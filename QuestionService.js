'use strict';

const db = require("./DatabaseInteraction/DatabaseInteraction.js");
const table ="question";

const schemaQuestion = require('./JSONschema/Question.json');
const jsontest = require("./TestJson.js");
const interactionTable="guest_interaction";

module.exports = {
    addQuestion: function(question, callback) {
        db.insert(table, question, callback);
    },
    deleteQuestion: function(keyValue, callback) {
        db.read(table, keyValue, function(err, res) {
            if(!err && res.dynamic===true) {
                db.remove(table, keyValue, callback);
            }else
                callback({Error:"U can't delete a dynamic question",TypeError:"deleteError"},null);
        });
    },
    getQuestions: function(limit, callback) {
        db.query(table, limit, null, null, callback);
    },
    updateQuestion: function(question, callback) {
        db.insert(table, question, callback);
    },
    getActions: function(limit, callback) {
        db.query(table, limit, null, null, callback);
    },
    getNextQuestion: function(question, callback) {
        db.read(table, {"id": question}, callback)
    },
    getFirstQuestion: function(callback) {
        db.query(table, 1, "isFirst = :first", {":first": true}, callback)
    },
    getLastAnswerText: function(callback) {
        db.query(table, 1, "id_nextQuestion = :last", {":last": null}, callback)
    },
    validateQuestion : function(obj) {
        return jsontest.test(schemaQuestion, obj);
    }
}